//var path = require("path");
//var argv = require("minimist");
//var fs = require("fs");
//var properties_reader = require('properties-reader');
//var properties = properties_reader

console.log('running....')

var newman = require('newman');

function testRun(){
    var optionsArray = {
        insecure: true,
        iterationCount: 1,
        delayRequest: 10000,
        timeoutRequest: 30000,
        environment: require('../environments/E2E-PY-Payment.postman_environment.json'),
        collection: require('../collections/3-Payment.postman_collection.json'),
        reporters:['cli','htmlextra','junit'],
        reporter:{htmlextra:{export: 'newman/index.html'}}
    };
    
    console.log(optionsArray);
    
    newman.run(optionsArray).on('start', function (err, args) { // on start of run, log to console
        console.log('running a collection...');
    }).on('done', function (err, summary) {
        if (err || summary.error) {
            console.error('collection run encountered an error.');
        }
        else {
            console.log('collection run completed.');
        }
    });
}

testRun();

//newman run collections/integration-service-flows.postman_collection -e environments/Integrations-service-flows-PY.postman_environment -r htmlextra --insecure